-- SUMMARY --
This module allow users with the appropriate permission, to visit a user's
profile and then, in addition to "Blocking" them, it should allow for the user
 to set 'Temporary Block' for a duration that automatically expires.

Modules permissions:
1. Temporarily Ban users - Allows for the selection of which roles can use
	this functionality
2. Exclude From Temporary Bans - Allows for certain roles to be excluded from
 banning. This is so an "Admin" or similar role cannot be banned by a lesser
 privileged role.

Lastly, there should ideally be a table that shows a listing of all users that
 are currently temporarily banned.

As an optional item, when a user who is temporarily banned attempts to login,
 the could be presented with a warning message.

Final note: I think the bans should be removed on Cron run. So it should be
 easy enough to simply set the expiration as a unix time stamp. Then, when
 Cron runs, all it has to do is look for time stamps that are lower than the
 current time. It then removes the bans.

This module is sponsored by ISPG
-- REQUIREMENTS --
  Date PopUp
-- INSTALLATION --

* Enable the module at Administer >> Site building >> Modules.
* Go to User Edit page which you want to block temporarely and add temporarily
 block users upto date.
-- CONTACT --

Current maintainers:
* Yogesh Saini (ISPG) -  http://drupal.org/user/725442
