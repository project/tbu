<?php
/**
 * @file
 * Allows administrators to block user by their assigned roles.
 */

/**
 * Implements hook_form_alter().
 */
function temporarily_block_roles_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'temporarily_block_users_settings') {

    drupal_add_css(drupal_get_path('module', 'temporarily_block_roles') . '/css/temporarily_block_roles_form.css');
    $form['user_block']['block_user_role'] = array(
      '#type' => 'fieldset',
      '#title' => t('Block users by roles wise'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['user_block']['block_user_role']['date'] = array(
      '#type' => 'markup',
      '#value' => t('Roles'),
      '#prefix' => '<div class="user-role-header"><div class="user-role-header-rolename">',
      '#suffix' => '</div>',
    );
    $form['user_block']['block_user_role']['name'] = array(
      '#type' => 'markup',
      '#value' => t('Date'),
      '#prefix' => '<div class="user-role-header-roledate">',
      '#suffix' => '</div></div>',
    );


    $roles = user_roles(TRUE);
    foreach ($roles as $id => $value) {
      if ($id == 2) {
        $value = 'All';
      }
      $blocktime = temporarily_block_roles_get_role_block_time($id);
      if ((time() >= strtotime(variable_get('temporarily_block_role_date_' . $id, time()))) || !variable_get('temporarily_block_role_' . $id, 0)) {
        $calc = 0;
      }
      else {
        $calc = 1;
      }

      $form['user_block']['block_user_role']['temporarily_block_role_' . $id] = array(
        '#type' => 'checkbox',
        '#title' => t($value),
        '#default_value' => $calc,
        '#prefix' => '<div class="user-role-date"><div class="user-role-checkbox">',
        '#suffix' => '</div>',
      );
      $form['user_block']['block_user_role']['temporarily_block_role_date_' . $id] = array(
        '#type' => 'date_popup',
        '#date_timezone' => date_default_timezone_name(),
        '#date_increment' => 1,
        '#date_year_range' => '0 : +19',
        '#default_value' => $calc ? variable_get('temporarily_block_role_date_' . $id, NOW) : NOW,
        '#prefix' => '<div class="user-role-date-field">',
        '#suffix' => '</div></div>',
      );
    }
    $form['#validate'][] = array('temporarily_block_users_settings_validate');
  }
}

/**
 * Function to validate entered dates.
 */
function temporarily_block_users_settings_validate($form, $form_state) {
  $roles = user_roles(TRUE);
  foreach ($roles as $id => $value) {
    if ($form_state['values']['temporarily_block_role_' . $id] == '1') {
      $blockdate = strtotime($form_state['values']['temporarily_block_role_date_' . $id]);
      if ($blockdate < time()) {
        form_set_error('temporarily_block_role_date_' . $id, 'Please select future date.');
        break;
      }
    }
  }
}

/**
 * Function to check users and block them if they set by admin expect admin 1.
 */
function temporarily_block_roles_temporarilyblock($form, $form_state) {
  $uname = $form_state['values']['name'];
  if (!temporarily_block_users_user_is_admin($uname)) {
    temporarily_block_roles_check_role_block($uname);
  }
}

/**
 * Function to check wheather user is admin 1 or not.
 */
function temporarily_block_users_user_is_admin($uname) {
  $account = user_load(array('name' => check_plain($uname)));
  if ($account->uid == 1) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Function to return blocked time.
 */
function temporarily_block_roles_get_role_block_time($roleid) {
  $role_date = variable_get('temporarily_block_role_date_' . $roleid, time());
  return strtotime($role_date);
}

/**
 * Function to check if particular user role is blocked or not.
 */
function temporarily_block_roles_check_role_block($username, $call = '1') {
  global $user;
  $roles = user_roles(TRUE);
  $role_id = array();
  $flag = FALSE;
  foreach ($roles as $id => $value) {
    $role_enabled = variable_get('temporarily_block_role_' . $id, 0);
    if ($role_enabled == 1) {
      $role_id[] = $id;
      if ($id == '2') {
        $flag = TRUE;
      }
    }
  }
  if ($flag) {
    $blocktime = temporarily_block_roles_get_role_block_time(2);
    if (time() < $blocktime || time() == $blocktime) {
      if ($call) {
        temporarily_block_users_login_error_message($username, $blocktime);
      }
      else {
        user_logout();
      }
      return;
    }
  }
  else {
    if (!empty($role_id)) {
      $user_rid = db_result(db_query('SELECT ur.rid urid FROM {users} u JOIN users_roles ur ON ur.uid=u.uid WHERE u.name ="%s" && ur.rid IN(' . db_placeholders($role_id, 'int') . ')', array_merge(array($username), $role_id)));
      if ($user_rid > 2) {
        $role_block_time = temporarily_block_roles_get_role_block_time($user_rid);
        if (time() < $role_block_time || time() == $role_block_time) {
          if ($call) {
            temporarily_block_users_login_error_message($username, $role_block_time);
          }
          else {
            user_logout();
          }
        }
      }
    }
  }
}
